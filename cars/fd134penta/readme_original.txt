================================================================
======================== Car Info ==============================
================================================================

Name				Fast Feline
Author				Flyboy
Date				October 16, 2019
Type				Original
Folder Name			beast
Rating				Pro
Class				Glow
Drivetrain			Four-wheel Drive
Top speed			38 mph
Body mass			1.4 kg

================================================================
======================= Description ============================
================================================================

Original car inspired by nineties sport cars such as the TVR Cerbera, Dodge Viper.


================================================================
======================== Credits ===============================
================================================================


Tools:
Gimp
Blender and Marv's plugin
Prm2Hul (By Jig)

================================================================
======================== Changelog =============================
================================================================

10/23/2019
Better hull

================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the authors wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.