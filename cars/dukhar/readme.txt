Car information
================================================================
Car name			: Dukhar

Car Type			: Original

Top speed			: 37.5mph (theoretical) / 64kph (observed)

Rating/Class		: 4 (pro)

Installed folder	: ...\cars\dukhar

Description			: Rallye Raid original car with hyper soft suspensions
					: Based off a comic called Calagan Rallye Raid



Author Information
================================================================
Author Name 		: Z3R0L33T

Email Address		: ts.guyot.0@free.fr

Other Info			: ReVolt user since 1999 !



Construction
================================================================
Base           		: Original concept car

Editor(s) used 		: Blender, Paint.net, Photoshop CS6



Additional Credits 
================================================================
Thanks to the whole community who continues to make ReVolt a fun and attractive game !



Copyright / Permissions
================================================================
You may do whatever you want with this car but you have to give me credits. Do not distribute without authorization.
 
