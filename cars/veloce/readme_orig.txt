================================================================
Car Information
================================================================
Car Name  : Lamborghini Diablo VT 5.7
Car Type  : Conversion
Folder	  : ...\cars\diablovt
Install   : Unzip with folder names on to the main ReVolt folder
Top speed : 50 mph
Rating    : Pro

================================================================
Author Information
================================================================
Author Name : Cat
EMail       : JZA80ToyotaSupraRZ@hotmail.com
Homepage    : http://www.freewebs.com/redpersiancat

================================================================
Car Description
================================================================
My favourite model and version of the Diablo, rather than the
late model with the fixed headlights.
Params are nothing special, just fast and AI friendly.

================================================================
Construction
================================================================
Base           : Lamborghini Diablo VT by Pitytour for NFS3,    
                 based off EA's Diablo SV.
Poly Count     : Low-poly, nuff said. 64 polys for each wheel.
Editor(s) used : NFS Wizard, Zmodeler 1.07b, prm2hul, RVshade,
                 RVsizer, Ulead Photo Express 1.0 and GIMP 2.6.
Known Bugs     : None

================================================================
Thanks And Accolades
================================================================
We want to thank:
.You, for downloading this car.
.All the RV community that keeps the game alive.
.The smart people at RVZT, ORP & RVL
                 (retarded non-english speaking noobs excluded).
.Skarma, for reinventing the NFS4 to RV tutorial.
.Pitytour & EA, for the original car.

================================================================
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars,
provided you give proper credit to the creators of the parts
from this car that were used to built the new car. E.g. if
you use the wheel mesh give me credit (i made it). 
 
You MAY distribute this CAR, provided you include this file,
with no modifications.  You may distribute this file in any
electronic format (BBS, Diskette, Flash Drive, Zip Drive, CD,
etc) as long as you include this file intact.

================================================================
Where to get this CAR
================================================================
www.rvzt.net
