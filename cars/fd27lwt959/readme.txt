﻿Car information
================================================================
Car name                : Acid Strike
Car Type  		: Original
Top speed 		: 43.5 mph
Rating/Class   		: Semi-pro
Installed folder       	: ...\cars\fd27lwt959

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.