{

;============================================================
;============================================================
; traxxas 
;============================================================
;============================================================

Name            "Stuntman"


;====================
; Model Filenames
;====================

MODEL    0      "cars\stntmonster\body.prm"
MODEL    1      "cars\stntmonster\L.prm"
MODEL    2      "cars\stntmonster\R.prm"
MODEL    3      "cars\stntmonster\springs.prm"
MODEL    4      "cars\stntmonster\axle.prm"
MODEL    5      "NONE"
MODEL    6      "NONE"
MODEL    7      "NONE"
MODEL    8      "NONE"
MODEL    9      "NONE"
MODEL    10     "NONE"
MODEL    11     "NONE"
MODEL    12     "NONE"
MODEL    13     "NONE"
MODEL    14     "NONE"
MODEL    15     "NONE"
MODEL    16     "NONE"
MODEL    17     "cars\misc\Aerial.m"
MODEL    18     "cars\misc\AerialT.m"
TPAGE   "cars\stntmonster\car.BMP"
TCARBOX "cars\stntmonster\carbox.bmp"
;)TSHADOW	"cars\stntmonster\shadow.bmp"
;)SHADOWTABLE	-78.3 78.3 73.4 -69.9 -10.93
COLL 	"cars\stntmonster\hull.hul"
EnvRGB  150 150 150
;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime        TRUE                           
Selectable      TRUE
;)STATISTICS TRUE
Class           1                               ; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain          0                               ; Obtain method
Rating          4                               ; Skill level (rookie, amateur, ...)
TopEnd     	3791.215189 			; Actual top speed (mph) for frontend bars
Acc             7.328700                        ; Acceleration rating (empirical)
Weight          2.990000                        ; Scaled weight (for frontend bars)
Handling        50.000000                       ; Handling ability (empirical and totally subjective)
Trans           0                               ; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	1.900000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed        42.000000                       ; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM             0.000000 -6.000000 -5.000000      ; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum        0                               ; Model Number in above list
Offset          0, 0, 0                      ; Calculated in game
Mass            2.990000
Inertia    	3000.000000 0.000000 0.000000
           	0.000000 8000.000000 0.000000
           	0.000000 0.000000 2500.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip            0.010000                        ; Converts downforce to friction value
StaticFriction  0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum        1
Offset1         -38.000000 6.750000 46.900000
Offset2         0.000000 0.000000 0.000000
IsPresent       TRUE
IsPowered       TRUE
IsTurnable      TRUE
SteerRatio  	-0.450000
EngineRatio 	39500.000000
Radius          19.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	12.000000
SkidWidth   	30.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.016000
StaticFriction  2.000000
KineticFriction 1.800000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum        2
Offset1         38.000000 6.750000 46.900000
Offset2         0.000000 0.000000 0.000000
IsPresent       TRUE
IsPowered       TRUE
IsTurnable      TRUE
SteerRatio  	-0.450000
EngineRatio 	39500.000000
Radius          19.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	12.000000
SkidWidth   	30.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.016000
StaticFriction  2.000000
KineticFriction 1.800000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum        1
Offset1         -38.400000 6.750000 -39.200000
Offset2         0.000000 0.000000 0.000000
IsPresent       TRUE
IsPowered       TRUE
IsTurnable      FALSE
SteerRatio  	0.100000
EngineRatio 	40550.000000
Radius          19.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	12.000000
SkidWidth   	30.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.016000
StaticFriction  2.100000
KineticFriction 1.80000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum        2
Offset1         38.400000 6.750000 -39.200000
Offset2         0.000000 0.000000 0.000000
IsPresent       TRUE
IsPowered       TRUE
IsTurnable      FALSE
SteerRatio  	0.100000
EngineRatio 	40550.000000
Radius          19.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	12.000000
SkidWidth   	30.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.016000
StaticFriction  2.100000
KineticFriction 1.80000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum        3
Offset          -12.000000 -30.500000 45.900000
Length          31.000000
Stiffness       250.000000
Damping         10.000000
Restitution     -0.960000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum        3
Offset          12.000000 -30.500000 45.900000
Length          31.000000
Stiffness       250.000000
Damping         10.000000
Restitution     -0.960000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum        3
Offset          -12.000000 -30.500000 -38.500000
Length          31.000000
Stiffness       250.000000
Damping         10.000000
Restitution     -0.960000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum        3
Offset          12.000000 -30.500000 -38.500000
Length          31.000000
Stiffness       250.000000
Damping         10.000000
Restitution     -0.960000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	-1.350000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	-1.350000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum        4
Offset          -9.605000 -4.000000 46.900000
Length          25.500000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum        4
Offset          9.605000 -4.00000 46.900000
Length          25.500000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum        4
Offset          -9.600000 -4.000000 -39.200000
Length          25.500000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum        4
Offset          9.600000 -4.000000 -39.200000
Length          25.500000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum        -1
Offset          0.000000 -13.000000 -8.000000
Axis            0.000000 0.000000 0.000000
AngVel          0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum     17
TopModelNum     18
Offset          -19.00000 -27.350000 30.550000
Direction       0.000000 -1.000000 0.000000
Length          28.000000
Stiffness       6000.000000
Damping         5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	93.820000
UnderRange  	3835.356934
UnderFront	 	197.100006
UnderRear   	556.727966
UnderMax    	0.510582
OverThresh  	462.209991
OverRange   	1182.744873
OverMax     	1.000000
OverAccThresh  	37.340000
OverAccRange   	625.908142
PickupBias     	13106
BlockBias      	22936
OvertakeBias   	19660
Suspension     	16383
Aggression     	0
}           	; End AI


}


