=============================
that-motor-freak's Freaky Creations
=============================

- MotoFreak, personal skin (that I'm still sharing anyway) -

HPI Vorza Flux by Hi-Ban
Repaint by yours truly
Livery pattern from GRID 2 (Codemasters, 2013)

Logos used (if anyone cares)
- HPI Racing, Vorza, Flux "F" logo
- Futaba
- Proline

--------------
Feel free to use these skins for whatever reason as long as you credit the authors.